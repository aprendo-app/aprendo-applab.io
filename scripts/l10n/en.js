export default {

chose_a_game:
"Chose a game",

fail_to_build_the_page:
"Fail to build the page.",

main_app_subheading:
"Educational Web Apps",

main_app_title:
"Aprendo",

writing_app_description:
"Listen and write some names with a keyboard.",

writing_app_name:
"Writing"

}
