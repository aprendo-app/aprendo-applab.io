const bubbles = []

function reset(bubble) {
  bubble.style.fontSize = (Math.random()+1) + 'rem'
  bubble.style.left = (Math.random()*110-5) + 'vw'
  bubble.y = 100
  bubble.incY = .2 + Math.random()/5
}

for (let i=0; i<10; i++) {
  let bubble = document.createElement('b')
  bubble.className = 'bubble'
  reset(bubble)
  bubble.y = Math.random()*120 + 80
  document.body.appendChild(bubble)
  bubbles.push(bubble)
}

setInterval(()=> bubbles.forEach(bubble => {
  bubble.y -= bubble.incY
  bubble.style.transform = `translate(0, ${bubble.y}vh)`
  if (bubble.y < -20) reset(bubble)
}), 33)
