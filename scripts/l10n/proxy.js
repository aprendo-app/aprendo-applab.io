import i18nBuilder from 'https://unpkg.com/@aurium/i18n@0.4.1/i18n.mjs'

import l10nEnData from './en.js'
import l10nPtData from './pt.js'

const l10nData = {
  en: l10nEnData,
  pt: l10nPtData,
}

export default i18nBuilder(l10nData)
