export default function notifyError(description) {
  if (description.str) description = description.str
  return (err)=> {
    console.error(description, err)
    alert(`Ups...\n\n${description}\n\n${err.message}`)
  }
}
