export default {

chose_a_game:
"Escolha um jogo",

fail_to_build_the_page:
"Falha na montagem da página.",

main_app_subheading:
"Web Apps Educacionais",

main_app_title:
"Aprendo",

writing_app_description:
"Escute e escreva palavras em um teclado.",

writing_app_name:
"Escrevendo"

}
