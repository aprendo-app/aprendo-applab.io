import elementMaker from './mkel.js'

export const mkEl = elementMaker

export function $(sel) {
  return elementMaker.extend(document.querySelector(sel))
}
