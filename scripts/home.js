import { $, mkEl } from './dom.js'
import notifyError from './notifyError.js'
import l10n from './l10n/proxy.js'
import './bubles.js'

const gameDirs = [ 'writing' ]

Promise.all(gameDirs.map(dir =>
  import(`/${dir}/metadata.js`).then(mod =>
    ({ ...mod.default, directory: dir })
  )
))
.then(buildPage)
.catch(notifyError(l10n.fail_to_build_the_page))

function buildPage(games) {
  const body = $('body')
  body.clear()
  body.mkChild('div', {
    class: 'app-root',
    child: [
      'header', [
        'h1', l10n.main_app_title,
        'strong', l10n.main_app_subheading
      ],
      'p', l10n.chose_a_game,
      'ul', { id: 'games', child: mkGameList(games) },
      'big', 'Work in progress...',
      'p', 'Langs: ' + l10n.$l10nLangs.join(', '),
      'p', 'Lang: ' + l10n.$lang
    ]
  })
  body.classList.remove('loading')
}

function mkGameList(games) {
  console.log(games)
  return games.flatMap(game => ['li', [
    'a', {
      href: `/${game.directory}/`,
      child: [
        'img', { src: `/${game.directory}/icon.svg` },
        'strong', game.name,
        'p', game.description
      ]
    }
  ]])
}
